<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Movie;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class DatabaseSeeder extends Seeder
{

    private $arrayPeliculas = array(
        array( 'title' => 'Spider-Man: No Way Home', 'year' => '2021-12-15', 'director' => 'Jon Watts', 'poster' => 'https://image.tmdb.org/t/p/w500/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg', 'rented' => false, 'synopsis' => 'Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.' ),
        array( 'title' => 'The Ice Age Adventures of Buck Wild', 'year' => '2022-01-28', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/zzXFM4FKDG7l1ufrAkwQYv2xvnh.jpg', 'rented' => false, 'synopsis' => 'The fearless one-eyed weasel Buck teams up with mischievous possum brothers Crash & Eddie as they head off on a new adventure into Bucks home: The Dinosaur World.' ),
        array( 'title' => 'Eternals', 'year' => '2021-11-03', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/bcCBq9N1EMo3daNIjWJ8kYvrQm6.jpg', 'rented' => false, 'synopsis' => 'The Eternals are a team of ancient aliens who have been living on Earth in secret for thousands of years. When an unexpected tragedy forces them out of the shadows, they are forced to reunite against mankind’s most ancient enemy, the Deviants.' ),
        array( 'title' => 'Encanto', 'year' => '2021-11-24', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/4j0PNHkMr5ax3IA8tjtxcmPU3QT.jpg', 'rented' => false, 'synopsis' => 'The tale of an extraordinary family, the Madrigals, who live hidden in the mountains of Colombia, in a magical house, in a vibrant town, in a wondrous, charmed place called an Encanto. The magic of the Encanto has blessed every child in the family with a unique gift from super strength to the power to heal—every child except one, Mirabel. But when she discovers that the magic surrounding the Encanto is in danger, Mirabel decides that she, the only ordinary Madrigal, might just be her exceptional familys last hope.' ),
        array( 'title' => 'Hotel Transylvania: Transformania', 'year' => '2022-01-13', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/teCy1egGQa0y8ULJvlrDHQKnxBL.jpg', 'rented' => false, 'synopsis' => 'When Van Helsings mysterious invention, the "Monsterfication Ray," goes haywire, Drac and his monster pals are all transformed into humans, and Johnny becomes a monster. In their new mismatched bodies, Drac and Johnny must team up and race across the globe to find a cure before its too late, and before they drive each other crazy.' ),
        array( 'title' => 'The Jack in the Box: Awakening', 'year' => '2022-01-03', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/3Ib8vlWTrAKRrTWUrTrZPOMW4jp.jpg', 'rented' => false, 'synopsis' => 'When a vintage Jack-in-the-box is opened by a dying woman, she enters into a deal with the demon within that would see her illness cured in return for helping it claim six innocent victims.' ), array( 'title' => 'Sing 2', 'year' => '2021-12-01', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/aWeKITRFbbwY8txG5uCj4rMCfSP.jpg', 'rented' => false, 'synopsis' => 'Buster and his new cast now have their sights set on debuting a new show at the Crystal Tower Theater in glamorous Redshore City. But with no connections, he and his singers must sneak into the Crystal Entertainment offices, run by the ruthless wolf mogul Jimmy Crystal, where the gang pitches the ridiculous idea of casting the lion rock legend Clay Calloway in their show. Buster must embark on a quest to find the now-isolated Clay and persuade him to return to the stage.' ),
        array( 'title' => 'Ghostbusters: Afterlife', 'year' => '2021-11-11', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/sg4xJaufDiQl7caFEskBtQXfD4x.jpg', 'rented' => false, 'synopsis' => 'When a single mom and her two kids arrive in a small town, they begin to discover their connection to the original Ghostbusters and the secret legacy their grandfather left behind.' ), array( 'title' => 'American Siege', 'year' => '2022-01-07', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/daeVrgyj0ue8qb3AHyU3UeCwoZz.jpg', 'rented' => false, 'synopsis' => 'An ex-NYPD officer-turned-sheriff of a small rural Georgia town has to contend with a gang of thieves who have taken a wealthy doctor hostage.' ),
        array( 'title' => 'One Shot', 'year' => '2021-11-05', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/3OXiTjU30gWtqxmx4BU9RVp2OTv.jpg', 'rented' => false, 'synopsis' => 'An elite squad of Navy SEALs, on a covert mission to transport a prisoner off a CIA black site island prison, are trapped when insurgents attack while trying to rescue the same prisoner.' ), array( 'title' => 'The Matrix Resurrections', 'year' => '2021-12-16', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/8c4a8kE7PizaGQQnditMmI1xbRp.jpg', 'rented' => false, 'synopsis' => 'Plagued by strange memories, Neos life takes an unexpected turn when he finds himself back inside the Matrix.' ),
        array( 'title' => 'Last Man Down', 'year' => '2021-10-19', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/4B7liCxNCZIZGONmAMkCnxVlZQV.jpg', 'rented' => false, 'synopsis' => 'After civilization succumbs to a deadly pandemic and his wife is murdered, a special forces soldier abandons his duty and becomes a hermit in the Nordic wilderness. Years later, a wounded woman appears on his doorstep. She escaped from a lab and her pursuers believe her blood is the key to a worldwide cure. Hes hesitant to get involved, but all doubts are cast aside when he discovers her pursuer is none other than Commander Stone, the man that murdered his wife some years ago.' ),
        array( 'title' => 'Resident Evil: Welcome to Raccoon City', 'year' => '2021-11-24', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/7uRbWOXxpWDMtnsd2PF3clu65jc.jpg', 'rented' => false, 'synopsis' => 'Once the booming home of pharmaceutical giant Umbrella Corporation, Raccoon City is now a dying Midwestern town. The company’s exodus left the city a wasteland…with great evil brewing below the surface. When that evil is unleashed, the townspeople are forever…changed…and a small group of survivors must work together to uncover the truth behind Umbrella and make it through the night.' ),
        array( 'title' => 'Red Notice', 'year' => '2021-11-04', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/lAXONuqg41NwUMuzMiFvicDET9Y.jpg', 'rented' => false, 'synopsis' => 'An Interpol-issued Red Notice is a global alert to hunt and capture the worlds most wanted. But when a daring heist brings together the FBIs top profiler and two rival criminals, theres no telling what will happen.' ), array( 'title' => 'Venom: Let There Be Carnage', 'year' => '2021-09-30', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg', 'rented' => false, 'synopsis' => 'After finding a host body in investigative reporter Eddie Brock, the alien symbiote must face a new enemy, Carnage, the alter ego of serial killer Cletus Kasady.' ),
        array( 'title' => 'The House', 'year' => '2022-01-14', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/iZjMFSKCrleKolC1gYcz5Rs8bk1.jpg', 'rented' => false, 'synopsis' => 'Across different eras, a poor family, an anxious developer and a fed-up landlady become tied to the same mysterious house in this animated dark comedy.' ), array( 'title' => 'Brazen', 'year' => '2022-01-13', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/7e4n1GfC9iky9VQzH3cDQz9wYpO.jpg', 'rented' => false, 'synopsis' => 'Mystery writer Grace Miller has killer instincts when it comes to motive - and shell need every bit of expertise to help solve her sisters murder.' ),
        array( 'title' => 'The Fallout', 'year' => '2022-01-28', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/3sQggeZUOte6JGMWIhO8BxlcjQh.jpg', 'rented' => false, 'synopsis' => 'In the wake of a school tragedy, Vada, Mia and Quinton form a unique and dynamic bond as they navigate the never linear, often confusing journey to heal in a world that feels forever changed.' ), array( 'title' => 'Tom and Jerry: Cowboy Up!', 'year' => '2022-01-24', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/muIaHotSaSUQr0KZCIJOYQEe7y2.jpg', 'rented' => false, 'synopsis' => 'This time, the rivals team up to help a cowgirl and her brother save their homestead from a greedy land-grabber, and they’re going to need some help! Jerry’s three precocious nephews are all ready for action, and Tom is rounding up a posse of prairie dogs. But can a ragtag band of varmints defeat a deceitful desperado determined to deceive a damsel in distress? No matter what happens with Tom and Jerry in the saddle, it’ll be a rootin’ tootin’ good time!' ),
        array( 'title' => 'Mother/Android', 'year' => '2021-12-17', 'director' => 'NaN', 'poster' => 'https://image.tmdb.org/t/p/w500/rO3nV9d1wzHEWsC7xgwxotjZQpM.jpg', 'rented' => false, 'synopsis' => 'Georgia and her boyfriend Sam go on a treacherous journey to escape their country, which is caught in an unexpected war with artificial intelligence. Days away from the arrival of their first child, the couple must face No Man’s Land—a stronghold of the android uprising—in hopes of reaching safety before giving birth.' )
    );

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        self::seedCatalog();
        self::seedUsers();
        $this->command->info('tabla catálogo iniializada con datos');
    }


    public function seedCatalog()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('movies')->delete();
        
        foreach( $this->arrayPeliculas as $pelicula ) {
            $p = new Movie;
            $p->title = $pelicula['title'];
            $p->year = $pelicula['year'];
            $p->director = $pelicula['director'];
            $p->poster = $pelicula['poster'];
            $p->rented = $pelicula['rented'];
            $p->synopsis = $pelicula['synopsis'];
            $p->save();
        }
    }


    public function seedUsers() {
        return User::create([
            'name' => 'luis',
            'email' => 'fdh.luis.angel@gmail.com',
            'password' => bcrypt('123'),
        ]);            

        $this->command->info('Tabla usuarios inicializada con datos!');
    }
}
