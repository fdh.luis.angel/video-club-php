@extends('layouts.master')

@section('content')
    
  <form method="post">
    {{ csrf_field() }}
    <div class="mb-3">
        <label for="title" class="form-label">Titulo</label>
        <input type="text" class="form-control" id="title" name="title">

        <label for="director" class="form-label">Director</label>
        <input type="text" class="form-control" id="director" name="director">

        <label for="year" class="form-label">Año</label>
        <input type="text" class="form-control" id="year" name="year">

        <label for="year" class="form-label">Poster/Imagen</label>
        <input type="text" class="form-control" id="poster" name="poster">

        <div class="form-group">
            <label for="synopsis">synopsis</label>
            <textarea class="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
        </div>

        <label for="rented" class="form-label">Rentada</label><input type="radio" name="rented">
    </div>

    <button type="submit" class="btn btn-primary">Enviar</button>
  </form>
@stop