@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-sm-4">
        <img src="{{$pelicula->poster}}" style="height:500px" />
    </div>
    <div class="col-sm-8">
        <div> <h3> {{$pelicula->title}} </h3> </div>
        <div> <h5> Año: {{$pelicula->year}}</h5> </div>
        <div> <h5> Director: {{$pelicula->director}} </h5> </div>
        <div style = "margin-top:40px"><p> <b>Resumen:</b> {{$pelicula->synopsis}} </p></div>
        <div style = "margin-top:40px">
            <p> <b>Estado:</b>
            @if ($pelicula['rented'])
                    Pelicula actualmente alquilada
                @else
                    Pelicula está disponible
            @endif
            </p>
        </div>

        <div style = "margin-top:40px">

            @if ($pelicula['rented'])
                <form action="{{ url('catalog/return', $pelicula->id) }}" method="POST" style="display:inline">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-secondary" style="display:inline">
                        <i class="bi bi-arrow-up-circle"></i> Devolver película
                    </button>
                </form>
            @else
                <form action="{{ url('catalog/rent', $pelicula->id) }}" method="POST" style="display:inline">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success" style="display:inline">
                        <i class="bi bi-credit-card-2-back"></i> Rentar película
                    </button>
                </form>
            @endif

            <a href="{{ url('/catalog/edit/' . $pelicula->id ) }}" class="btn btn-warning"><i class="bi bi-pencil-fill"></i> Editar pelicula</a>
            <a class="btn btn-outline-secondary">  Volver al listado</a>

            <form action="{{ url('catalog/delete', $pelicula->id) }}" method="POST" style="display:inline">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger" style="display:inline">
                    <i class="bi bi-trash-fill"></i> Eliminar
                </button>
            </form>
        </div>
    </div>
</div>
@stop