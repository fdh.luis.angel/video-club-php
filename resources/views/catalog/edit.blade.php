@extends('layouts.master')

@section('content')
<form method="post">
    {{method_field('PUT')}}
    {{ csrf_field() }}
    <div class="mb-3">
        <label for="title" class="form-label">Titulo</label>
        <input type="text" class="form-control" id="title" name="title" value="{{$pelicula->title}}">

        <label for="director" class="form-label">Director</label>
        <input type="text" class="form-control" id="director" name="director" value="{{$pelicula->director}}">

        <label for="year" class="form-label">Año</label>
        <input type="text" class="form-control" id="year" name="year" value="{{$pelicula->year}}">

        <label for="year" class="form-label">Poster/Imagen</label>
        <input type="text" class="form-control" id="poster" name="poster" value="{{$pelicula->poster}}">

        <div class="form-group">
            <label for="synopsis">synopsis</label>
            <textarea class="form-control" id="synopsis" rows="3" name="synopsis">{{$pelicula->synopsis}}</textarea>
        </div>

        <label for="rented" class="form-label">Rentada</label><input type="checkbox" name="rented">
    </div>

    <button type="submit" class="btn btn-primary">Modificar película</button>
  </form>
@stop