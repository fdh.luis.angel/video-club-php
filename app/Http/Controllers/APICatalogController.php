<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;

class APICatalogController extends Controller
{
    public function index(){
        return response()->json( Movie::all() );
    }

    public function show($id){
        $movie = Movie::findOrFail($id);
        return response()->json( $movie );
    }

    public function store(Request $request){
        $p = new Movie;
        $p->title = $request->input('title');
        $p->year = $request->input('year');
        $p->director = $request->input('director');
        $p->poster = $request->input('poster');
        $p->rented = false;
        $p->synopsis = $request->input('synopsis'); 
        $p->save();

        return response()->json( ['error' => false,
        'msg' => 'Pelicula creada' ] );
    }

    public function update(Request $request, $id){
        $p = Movie::findOrFail($id);
        $p->title = $request->input('title');
        $p->year = $request->input('year');
        $p->director = $request->input('director');
        $p->poster = $request->input('poster');
        $p->poster = $request->input('rented');
        $p->synopsis = $request->input('synopsis'); 
        $p->update();

        return response()->json( ['error' => false,
        'msg' => 'Pelicula actualizada' ] );
    }

    public function destroy($id){
        $p = Movie::findOrFail($id);
        $p->delete();
        notify()->success('Pelicula eliminada con éxito');
        return response()->json( ['error' => false,
        'msg' => 'Pelicula eliminada' ] );
    }
    
    public function putRent($id){
        $m = Movie::findOrFail( $id );
        $m->rented = true;
        $m->save();
        return response()->json( ['error' => false,
        'msg' => 'La película se ha marcado como alquilada' ] );
    }

    public function putReturn($id){
        $m = Movie::findOrFail( $id );
        $m->rented = true;
        $m->save();
        return response()->json( ['error' => false,
        'msg' => 'La película regresada' ] );
    }
}
