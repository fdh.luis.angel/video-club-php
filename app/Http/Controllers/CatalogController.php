<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mckenziearts\Notify\LaravelNotify;

class CatalogController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex(){
        /*
        $movies = DB::table('movies')->get();
        $arrayPeliculas = array();

        foreach ($movies as $movie)
        {
            $tempMovie = array( 
                'title' => $movie->title,
                'year' => $movie->year,
                'director' => $movie->director,
                'poster' => $movie->poster,
                'rented' => $movie->rented, 
                'synopsis' => $movie->synopsis
            );

            array_push($arrayPeliculas, $tempMovie);
        }
        */

        $arrayPeliculas = Movie::all();

        return view('catalog.index', array('arrayPeliculas' => $arrayPeliculas));
    }


    public function getShow($id){
        $movie = Movie::findOrFail($id);
        return view('catalog.show', array('pelicula'=>$movie));
    }

    public function getCreate(){
        return view('catalog.create');
    }

    public function postCreate(Request $request){
        $p = new Movie;
        $p->title = $request->input('title');
        $p->year = $request->input('year');
        $p->director = $request->input('director');
        $p->poster = $request->input('poster');
        $p->rented = false;
        $p->synopsis = $request->input('synopsis'); 
        $p->save();

        notify()->success('Laravel Notify is awesome!');

        return redirect('/catalog');
    }

    public function getEdit($id){
        $movie = Movie::findOrFail($id);
        return view('catalog.edit', array('pelicula'=>$movie));
    }

    public function putEdit(Request $request, $id){
        $p = Movie::findOrFail($id);
        $p->title = $request->input('title');
        $p->year = $request->input('year');
        $p->director = $request->input('director');
        $p->poster = $request->input('poster');
        if ($request->has('rented')){
            $p->rented = true;
        } else {
            $p->rented = false;
        }
        $p->synopsis = $request->input('synopsis'); 
        $p->update();
        return redirect('/catalog');
    }

    public function putRent(Request $request, $id){
        $p = Movie::findOrFail($id);
        $p->rented = true;
        $p->update();
        notify()->success('Pelicula rentada');
        return redirect('/catalog/show/'.$id );
    }

    public function putReturn(Request $request, $id){
        $p = Movie::findOrFail($id);
        $p->rented = false;
        $p->update();
        notify()->success('Pelicula regresada');
        return redirect('/catalog/show/'.$id );
    }

    public function deleteMovie(Request $request, $id){
        $p = Movie::findOrFail($id);
        $p->delete();
        notify()->success('Pelicula eliminada con éxito');
        return redirect('/catalog');
    }
    
}
